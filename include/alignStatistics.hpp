/***********************************************************************
* Licensed Materials - Property of Lorenzo Gatti
* Copyright (C) 2015-2019 by Lorenzo Gatti
************************************************************************
* This file is part of AliSt, a computer program whose purpose is to
* compute statistics on multi-sequence alignments.
*
* This software is based and extends the following libraries:
* - the Bio++ libraries
*   developed by the Bio++ Development Team <http://biopp.univ-montp2.fr>
*
* AliSt is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.
*
* AliST is a free software: you can redistribute it and/or modify it
* under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* You should have received a copy of the GNU Affero General Public
* License along with AliST. If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/**
 * @file PID.hpp
 * @author Lorenzo Gatti
 * @date 18 07 2018
 * @version 1.0
 * @maintainer Lorenzo Gatti
 * @email lg@lorenzogatti.me
 * @status Development
 *
 * @brief
 * @details
 * @pre
 * @bug
 * @warning
 *
 * @see For more information visit: http://www.lorenzogatti.me
 */
#ifndef ALIST_PID_HPP
#define ALIST_PID_HPP

#include <Bpp/Seq/Container/SiteContainer.h>

namespace bpp {

/*!
 * @brief Calculates the percent sequence identity for a pairwise sequence alignment.
 *
 *   "PID1": (identical positions) / (aligned positions + internal gap positions)
 *   "PID2": (identical positions) / (aligned positions)
 *   "PID3": (identical positions) / (aligned_char + aligned_identical)
 *   "PID4": (identical positions) / (aligned_char + aligned_gap + aligned_identical)
 */
    class alignStatistics {
    public:
        enum type {
            PID1 = 1, PID2 = 2, PID3 = 3, PID4 = 4
        };

        enum siteClass {identical, aligned_char, aligned_gap, full_gap};

    private:
        SiteContainer *data;
        alignStatistics::type method;

        std::map<int,std::map<int,std::map<alignStatistics::siteClass,double>>> pairSiteCounts_;
        std::map<size_t,std::map<int,int>> indelDistribution_;

        std::vector<std::vector<double>> PID_;

        std::vector<double> CScoreSequences_;
        std::vector<double> CScoreSites_;
        std::map<size_t, std::map<size_t, double>> CScorePairwise_;
        std::map<size_t, std::map<size_t, double>> ICScorePairwise_;
        //std::map<size_t,std::map<size_t,double>> CScorePairwise_;

        int totalNumberOfUnambiguosChars_;
        double meanPID_;
        double meanGapProportion_;
        double meanCharProportion_;
        double CScoreAlignment_;

    public:
        alignStatistics(SiteContainer &sites, type pidMethod);

        ~alignStatistics() = default;

        SiteContainer *getData() const {
            return data;
        }

        void setData(SiteContainer *in_data) {
            alignStatistics::data = in_data;
        }

        type getMethod() const {
            return method;
        }

        void setMethod(type in_method) {
            alignStatistics::method = in_method;
        }

        double getMeanPID(){
            return meanPID_;
        }
        double getMeanGapProportion(){
            return meanGapProportion_;
        }
        double getMeanCharProportion(){
            return meanCharProportion_;
        }

        void computeStatistics();

        void computeIndelDistributionAlignment();

        void computeCScoreAlignment();

        void printPIDMatrix();

        void printIndelDistribution();

        double getCScoreAlignment() const {
            return CScoreAlignment_;
        }

        double getCr_max() const {

            return *std::max_element(CScoreSequences_.begin(), CScoreSequences_.end());
        }

        double getCr_min() const {

            return *std::min_element(CScoreSequences_.begin(), CScoreSequences_.end());
        }

        double getCc_max() const {

            return *std::max_element(CScoreSites_.begin(), CScoreSites_.end());
        }

        double getCc_min() const {

            return *std::min_element(CScoreSites_.begin(), CScoreSites_.end());
        }

        std::vector<double> getValuesMap(std::map<size_t, std::map<size_t, double>> inmap) const {

            std::vector<double> values;
            size_t numSeqs = data->getNumberOfSequences();

            for (size_t s1 = 0; s1 < numSeqs; s1++) {
                for (size_t s2 = 0; s2 < numSeqs; s2++) {
                    values.push_back(inmap[s1][s2]);
                }
            }
            return values;
        }

        double getCij_min() const {
            std::vector<double> tmp = getValuesMap(CScorePairwise_);
            return *std::min_element(tmp.begin(), tmp.end());
        }

        double getCij_max() const {
            std::vector<double> tmp = getValuesMap(CScorePairwise_);
            return *std::max_element(tmp.begin(), tmp.end());
        }

        double getIij_min() const {
            std::vector<double> tmp = getValuesMap(ICScorePairwise_);
            return *std::min_element(tmp.begin(), tmp.end());
        }

        double getIij_max() const {
            std::vector<double> tmp = getValuesMap(ICScorePairwise_);
            return *std::max_element(tmp.begin(), tmp.end());
        }

        const std::vector<double> &getCScoreSequences() const {
            return CScoreSequences_;
        }

        const std::vector<double> &getCScoreSites() const {
            return CScoreSites_;
        }

        const double getCScorePairwise(size_t s1, size_t s2) const {
            return CScorePairwise_.at(s1).at(s2);
        }

        const double getICScorePairwise(size_t s1, size_t s2) const {
            return ICScorePairwise_.at(s1).at(s2);
        }

        const double getPID(size_t s1, size_t s2) const {
            return PID_.at(s1).at(s2);
        }

        const std::map<size_t, std::map<int, int>> &getIndelDistribution_() const {
            return indelDistribution_;
        }

    protected:

        void computeIndelDistributionForASequence(size_t seqID);

        void computePairwiseSiteStatistics();

        void computeSiteClassesForTwoSequences(size_t seq1, size_t seq2);

        void computeSiteStatistics();

        void computeCScoreForASequence(size_t seqID);

        void computeCScoreForASite(size_t siteID);

        void computeCScoreForTwoSequences(size_t seq1, size_t seq2);

    };



}
#endif //ALIST_PID_HPP
