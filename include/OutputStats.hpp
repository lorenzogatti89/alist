/***********************************************************************
* Licensed Materials - Property of Lorenzo Gatti
* Copyright (C) 2015-2019 by Lorenzo Gatti
************************************************************************
* This file is part of AliSt, a computer program whose purpose is to
* compute statistics on multi-sequence alignments.
*
* This software is based and extends the following libraries:
* - the Bio++ libraries
*   developed by the Bio++ Development Team <http://biopp.univ-montp2.fr>
*
* AliSt is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.
*
* AliST is a free software: you can redistribute it and/or modify it
* under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* You should have received a copy of the GNU Affero General Public
* License along with AliST. If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/**
 * @file OutputStats.hpp
 * @author Lorenzo Gatti
 * @date 19 07 2018
 * @version 1.0
 * @maintainer Lorenzo Gatti
 * @email lg@lorenzogatti.me
 * @status Development
 *
 * @brief
 * @details
 * @pre
 * @bug
 * @warning
 *
 * @see For more information visit: http://www.lorenzogatti.me
 */
#ifndef ALIST_OUTPUTSTATS_HPP
#define ALIST_OUTPUTSTATS_HPP


#include "alignStatistics.hpp"

namespace OutputStats {

    void export2csv(bpp::alignStatistics *stats, std::string fileName, std::string argOutputPrefix, std::string execPath);


};


#endif //ALIST_OUTPUTSTATS_HPP
