/***********************************************************************
* Licensed Materials - Property of Lorenzo Gatti
* Copyright (C) 2015-2019 by Lorenzo Gatti
************************************************************************
* This file is part of AliSt, a computer program whose purpose is to
* compute statistics on multi-sequence alignments.
*
* This software is based and extends the following libraries:
* - the Bio++ libraries
*   developed by the Bio++ Development Team <http://biopp.univ-montp2.fr>
*
* AliSt is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.
*
* AliST is a free software: you can redistribute it and/or modify it
* under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* You should have received a copy of the GNU Affero General Public
* License along with AliST. If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/**
 * @file AlistApplication.hpp
 * @author Lorenzo Gatti
 * @date 18 07 2018
 * @version 1.0
 * @maintainer Lorenzo Gatti
 * @email lg@lorenzogatti.me
 * @status Development
 *
 * @brief
 * @details
 * @pre
 * @bug
 * @warning
 *
 * @see For more information visit: http://www.lorenzogatti.me
 */
#ifndef ALIST_ALISTAPPLICATION_HPP
#define ALIST_ALISTAPPLICATION_HPP
// From the STL:
#include <string>
#include <map>
#include <Bpp/Exceptions.h>
#include <glog/logging.h>
#include <iostream>
#include <Bpp/App/ApplicationTools.h>

#include <boost/asio/ip/host_name.hpp>

namespace bpp {
    class AlistApplication {

    private:
        std::string appName_;
        std::string appBuild_;
        std::string appVersion_;
        mutable std::map<std::string, std::string> params_;
        bool timerStarted_;
        long seed_;

    public:
        AlistApplication(int argc, char *argv[], const std::string &name, const std::string &strVersion, const std::string &build_date);

    public:
        void startTimer();

        void done();

        std::map<std::string, std::string> &getParams() { return params_; }

        const std::string &getParam(const std::string &name) const {
            if (params_.find(name) == params_.end()) throw bpp::Exception("BppApplication::getParam(). Parameter '" + name + "' not found.");
            return params_[name];
        }

        std::string &getParam(const std::string &name) { return params_[name]; }

        long getSeed() { return seed_; }

        void help() {
            std::cout << appName_ << std::endl << std::endl;
            std::cout << "Usage: AliSt [arguments] or [params=file.txt]" << std::endl;
            std::cout << "Documentation can be found at https://bitbucket.org/lorenzogatti89/AliSt/" << std::endl;
        }


        void banner() {

            auto host_name = boost::asio::ip::host_name();

            bpp::ApplicationTools::displayMessage("------------------------------------------------------------------------------");
            bpp::ApplicationTools::displayMessage(appName_);
            bpp::ApplicationTools::displayMessage("Multi-Sequence Alignment Statistics");
            bpp::ApplicationTools::displayMessage("Authors: Lorenzo Gatti");
            bpp::ApplicationTools::displayMessage("Build on commit: " + appVersion_);
            bpp::ApplicationTools::displayMessage("On date: " + appBuild_);
            bpp::ApplicationTools::displayMessage("------------------------------------------------------------------------------");
            bpp::ApplicationTools::displayResult("Execution started on:", host_name);


        }

        void version() {
            std::cout << appName_ << std::endl;
            std::cout << appVersion_ << std::endl;
            std::cout << appBuild_ << std::endl;
        }

    };

}
#endif //ALIST_ALISTAPPLICATION_HPP
