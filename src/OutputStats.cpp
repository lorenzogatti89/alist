/***********************************************************************
* Licensed Materials - Property of Lorenzo Gatti
* Copyright (C) 2015-2019 by Lorenzo Gatti
************************************************************************
* This file is part of AliSt, a computer program whose purpose is to
* compute statistics on multi-sequence alignments.
*
* This software is based and extends the following libraries:
* - the Bio++ libraries
*   developed by the Bio++ Development Team <http://biopp.univ-montp2.fr>
*
* AliSt is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.
*
* AliST is a free software: you can redistribute it and/or modify it
* under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* You should have received a copy of the GNU Affero General Public
* License along with AliST. If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/**
 * @file OutputStats.cpp
 * @author Lorenzo Gatti
 * @date 19 07 2018
 * @version 1.0
 * @maintainer Lorenzo Gatti
 * @email lg@lorenzogatti.me
 * @status Development
 *
 * @brief
 * @details
 * @pre
 * @bug
 * @warning
 *
 * @see For more information visit: http://www.lorenzogatti.me
 */
#include "OutputStats.hpp"


using namespace bpp;

void OutputStats::export2csv(bpp::alignStatistics *stats, std::string fileName, std::string argOutputPrefix, std::string execPath) {

    // Initialization variables for output
    std::string stemPath = execPath + "/" + fileName + "." + argOutputPrefix;
    std::ofstream ofs;
    std::string filename;
    std::string delimiter = ",";

    size_t numSeqs = stats->getData()->getNumberOfSequences();
    size_t numSites = stats->getData()->getNumberOfSites();

    // -----------------------------------------------
    // Table 1 - completeness scores for individual sequences (Cr)
    filename = stemPath + "cscores_seqs_cr.csv";
    ApplicationTools::displayResult("C-Scores (Cr) for individual sequences", filename);

    ofs.open(filename, std::ofstream::out);

    ofs << "SeqID,SeqName,Sites,Cr" << std::endl;
    for (size_t s = 0; s < numSeqs; s++) {
        std::string seqName = stats->getData()->getSequence(s).getName();
        size_t sites = stats->getData()->getSequence(s).size();
        ofs << s << "," << seqName << "," << sites << "," << stats->getCScoreSequences()[s] << std::endl;
    }
    ofs.close();

    // -----------------------------------------------
    // Table 2 - completeness scores for individual sites (Cc)
    filename = stemPath + "cscores_sites_cc.csv";
    ApplicationTools::displayResult("C-Scores (Cc) for sites", filename);

    ofs.open(filename, std::ofstream::out);
    ofs << "SiteID,Cc" << std::endl;
    for (size_t s = 0; s < numSites; s++) {
        ofs << s << "," << stats->getCScoreSites()[s] << std::endl;
    }
    ofs.close();

    // -----------------------------------------------
    // Table 4 - Matrix with C scores for pairs of sequences (Cij)
    filename = stemPath + "cscores_pairwise_cij.csv";
    ApplicationTools::displayResult("C-Scores (Cij) pairwise", filename);

    ofs.open(filename, std::ofstream::out);
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        std::string seqName = stats->getData()->getSequence(s1).getName();
        ofs << seqName;
        for (size_t s2 = 0; s2 < numSeqs; s2++) {
            ofs << "," << stats->getCScorePairwise(s1, s2);
        }
        ofs << std::endl;
    }
    ofs.close();

    // -----------------------------------------------
    // Table 5 - Matrix with incompleteness scores for pairs of sequences (Iij)
    filename = stemPath + "iscores_pairwise_Iij.csv";
    ApplicationTools::displayResult("I-Scores (Iij) pairwise", filename);

    ofs.open(filename, std::ofstream::out);
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        std::string seqName = stats->getData()->getSequence(s1).getName();
        ofs << seqName;
        for (size_t s2 = 0; s2 < numSeqs; s2++) {
            ofs << "," << stats->getICScorePairwise(s1, s2);
        }
        ofs << std::endl;
    }
    ofs.close();

    // -----------------------------------------------
    // Table 6 - Table with completeness scores (Cij) and incompleteness scores (Iij)
    filename = stemPath + "table_pairwise_Cij_Iij.csv";
    ApplicationTools::displayResult("C/I-Scores (C/Iij) pairwise", filename);

    ofs.open(filename, std::ofstream::out);
    ofs << "Taxon1,Taxon2,Cij,Iij" << std::endl;
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        for (size_t s2 = 0; s2 < numSeqs; s2++) {
            ofs << stats->getData()->getSequence(s1).getName() << "," <<
                stats->getData()->getSequence(s2).getName() << "," <<
                stats->getCScorePairwise(s1, s2) << "," <<
                stats->getICScorePairwise(s1, s2) << std::endl;
        }
    }
    ofs.close();

    // -----------------------------------------------
    // Table 7 - PID
    filename = stemPath + "table_pairwise_identities.csv";
    ApplicationTools::displayResult("Pairwise Identity Matrix (PID)", filename);

    ofs.open(filename, std::ofstream::out);
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        if (s1 == numSeqs - 1) { delimiter = ""; } else { delimiter = ","; };
        ofs << stats->getData()->getSequence(s1).getName() << delimiter;
    }
    ofs << std::endl;
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        ofs << stats->getData()->getSequence(s1).getName() << ",";
        for (size_t s2 = 0; s2 < numSeqs - 1; s2++) {
            if (s2 == (numSeqs - 2)) { delimiter = ""; } else { delimiter = ","; };
            if (s1 != s2) {
                ofs << std::setprecision(5) << stats->getPID(s1, s2);
            } else {
                ofs << std::setprecision(5) << "1.0";
            }
            ofs << delimiter;
        }
        ofs << std::endl;
    }

    ofs.close();

    // -----------------------------------------------
    // Table 8 - Indel Distribution
    filename = stemPath + "indel_distribution.csv";
    ApplicationTools::displayResult("InDel distribution ", filename);

    ofs.open(filename, std::ofstream::out);

    std::map<size_t, std::map<int, int>> indels = stats->getIndelDistribution_();

    double totalIndels = 0;
    std::map<int, int> summaryIndelDistribution;
    for (size_t i = 0; i < numSeqs; i++) {
        for (std::map<int, int>::iterator it = indels[i].begin(); it != indels[i].end(); ++it) {
            summaryIndelDistribution[it->first]++;
        }
    }
    ofs << "class" << ",";
    for (std::map<int, int>::iterator it = summaryIndelDistribution.begin(); it != summaryIndelDistribution.end(); ++it) {
        if (it->first != 0) {
            if (std::next(it) == summaryIndelDistribution.end()) { delimiter = ""; } else { delimiter = ","; };
            std::string header = "[" + std::to_string(it->first) + "]";
            ofs << header << delimiter;
        }
    }
    ofs << std::endl;
    ofs << "counts" << ",";
    for (std::map<int, int>::iterator it = summaryIndelDistribution.begin(); it != summaryIndelDistribution.end(); ++it) {
        if (it->first != 0) {
            if (std::next(it) == summaryIndelDistribution.end()) { delimiter = ""; } else { delimiter = ","; };
            ofs << it->second << delimiter;
            totalIndels += it->second;
        }
    }
    ofs << std::endl;
    ofs << "prop" << ",";
    for (std::map<int, int>::iterator it = summaryIndelDistribution.begin(); it != summaryIndelDistribution.end(); ++it) {
        if (it->first != 0) {
            if (std::next(it) == summaryIndelDistribution.end()) { delimiter = ""; } else { delimiter = ","; };
            ofs << it->second / totalIndels << delimiter;
        }
    }
    ofs << std::endl;

    ofs.close();

    // -----------------------------------------------
    // Table 9 - Statistics MSA
    filename = stemPath + "stats.csv";
    ofs.open(filename, std::ofstream::out);
    ApplicationTools::displayResult("Summary statistics ", filename);
    delimiter = ",";
    ofs << "noSeq,noSites,noPairs,meanPID,meanGap,meanChar,Ca,Cr_max,Cr_min,Cc_max,Cc_min,Cij_max,Cij_min,Iij_max,Iij_min" << std::endl;

    size_t seqpairs = numSeqs * (numSeqs - 1) / 2;

    ofs << numSeqs << delimiter <<
        numSites << delimiter <<
        seqpairs << delimiter <<
        stats->getMeanPID() << delimiter <<
        stats->getMeanGapProportion() << delimiter <<
        stats->getCScoreAlignment() << delimiter <<
        stats->getCr_max() << delimiter <<
        stats->getCr_min() << delimiter <<
        stats->getCc_max() << delimiter <<
        stats->getCc_min() << delimiter <<
        stats->getCij_max() << delimiter <<
        stats->getCij_min() << delimiter <<
        stats->getIij_max() << delimiter <<
        stats->getIij_min() << std::endl;

    ofs.close();


}
