/***********************************************************************
* Licensed Materials - Property of Lorenzo Gatti
* Copyright (C) 2015-2019 by Lorenzo Gatti
************************************************************************
* This file is part of AliSt, a computer program whose purpose is to
* compute statistics on multi-sequence alignments.
*
* This software is based and extends the following libraries:
* - the Bio++ libraries
*   developed by the Bio++ Development Team <http://biopp.univ-montp2.fr>
*
* AliSt is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.
*
* AliST is a free software: you can redistribute it and/or modify it
* under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* You should have received a copy of the GNU Affero General Public
* License along with AliST. If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/**
 * @file PID.cpp
 * @author Lorenzo Gatti
 * @date 18 07 2018
 * @version 1.0
 * @maintainer Lorenzo Gatti
 * @email lg@lorenzogatti.me
 * @status Development
 *
 * @brief
 * @details
 * @pre
 * @bug
 * @warning
 *
 * @see For more information visit: http://www.lorenzogatti.me
 */
#include "alignStatistics.hpp"
#include <iomanip>
#include <iostream>
#include <regex>

bpp::alignStatistics::alignStatistics(bpp::SiteContainer &sites, type pidMethod) {

    setData(&sites);
    setMethod(pidMethod);

    //

    totalNumberOfUnambiguosChars_ = 0;


    // Initialise counting map
    for (size_t s1 = 0; s1 < data->getNumberOfSequences(); s1++) {
        indelDistribution_[s1].insert(std::make_pair(0,0));

        for (size_t s2 = 0; s2 < data->getNumberOfSequences(); s2++) {
            if (s1 != s2) {
                pairSiteCounts_[s1][s2].insert(std::make_pair(siteClass::identical, 0));
                pairSiteCounts_[s1][s2].insert(std::make_pair(siteClass::aligned_char, 0));
                pairSiteCounts_[s1][s2].insert(std::make_pair(siteClass::aligned_gap, 0));
                pairSiteCounts_[s1][s2].insert(std::make_pair(siteClass::full_gap, 0));
            }
            CScorePairwise_[s1][s2] = 0;
            ICScorePairwise_[s1][s2] = 0;
        }
    }


    PID_.resize(data->getNumberOfSequences());
    CScoreSequences_.resize(data->getNumberOfSequences());
    CScoreSites_.resize(data->getNumberOfSites());
    for (size_t i = 0; i < data->getNumberOfSequences(); i++)
        PID_[i].resize(data->getNumberOfSequences() - 1);


    meanPID_ = 0;
    meanGapProportion_ = 0;
    meanCharProportion_ = 0;
}

void bpp::alignStatistics::computeStatistics() {

    computePairwiseSiteStatistics();
    computeSiteStatistics();


    size_t numSeqs = data->getNumberOfSequences();
    size_t numSites = data->getNumberOfSites();
    double identicalSites;

    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        for (size_t s2 = 0; s2 < numSeqs; s2++) {
            if (s1 != s2) {

                identicalSites = pairSiteCounts_[s1][s2][siteClass::identical];

                switch(method){
                    case type::PID1:
                        PID_[s1][s2] = (identicalSites / (double) numSites);
                        break;
                    case type::PID2:
                        numSites = pairSiteCounts_[s1][s2][siteClass::aligned_char];
                        PID_[s1][s2] = (identicalSites / (double) numSites);
                        break;
                    case type::PID3:
                        numSites = pairSiteCounts_[s1][s2][siteClass::aligned_char] + pairSiteCounts_[s1][s2][siteClass::identical];
                        PID_[s1][s2] = (identicalSites / (double) numSites);
                        break;
                    case type::PID4:
                        numSites = pairSiteCounts_[s1][s2][siteClass::aligned_char] +
                                              pairSiteCounts_[s1][s2][siteClass::aligned_gap] +
                                              pairSiteCounts_[s1][s2][siteClass::identical];
                        PID_[s1][s2] = (identicalSites / (double) numSites);
                        break;
                    default:
                        ApplicationTools::displayError("PID method not implemented!");

                }
                // upper triangle
                if(s2>s1) {
                    meanPID_ += PID_[s1][s2];
                }
            }
        }
    }

    meanPID_ /= (numSeqs*(numSeqs-1)/2);





}

void bpp::alignStatistics::computePairwiseSiteStatistics() {
    for (size_t id1 = 0; id1 < data->getNumberOfSequences(); id1++) {
        for (size_t id2 = 0; id2 < data->getNumberOfSequences(); id2++) {
            computeSiteClassesForTwoSequences(id1, id2);
        }
    }

}

void bpp::alignStatistics::computeSiteClassesForTwoSequences(size_t seq1, size_t seq2) {
    // Skip identical seqs
    if (seq1 != seq2) {
        int gapChar = data->getAlphabet()->getGapCharacterCode();
        // Get mapped state from site container
        for (size_t p = 0; p < data->getNumberOfSites(); p++) {

            int state1 = data->getSite(p).getValue(seq1);
            int state2 = data->getSite(p).getValue(seq2);

            if (state1 == state2) {
                pairSiteCounts_[seq1][seq2][siteClass::identical]++;
            } else if (state1 == gapChar && state2 == gapChar) {
                pairSiteCounts_[seq1][seq2][siteClass::full_gap]++;
            } else if (state1 != state2 && state1 != gapChar && state2 != gapChar) {
                pairSiteCounts_[seq1][seq2][siteClass::aligned_char]++;
            } else {
                pairSiteCounts_[seq1][seq2][siteClass::aligned_gap]++;
            }

        }
    }
}

void bpp::alignStatistics::printPIDMatrix() {

    size_t numSeqs = data->getNumberOfSequences();

    std::cout << "\t" ;
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        std::cout << std::setw(10) << data->getSequence(s1).getName();
    }
    std::cout << std::endl;
    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        std::cout << data->getSequence(s1).getName() << "\t";
        for (size_t s2 = 0; s2 < numSeqs; s2++) {
            if (s1 != s2) {
                std::cout << std::setw(10) << std::setprecision(5) << PID_[s1][s2];
            }else{
                std::cout << std::setw(10) << std::setprecision(5) << "1.0";
            }
        }
        std::cout << std::endl;
    }
}

void bpp::alignStatistics::computeSiteStatistics() {
    size_t numSeqs = data->getNumberOfSequences();
    size_t numSites = data->getNumberOfSites();
    int gapChar = data->getAlphabet()->getGapCharacterCode();

    double totalCharacters = (numSites*numSeqs);

    for (size_t p = 0; p < numSites; p++) {
        for (size_t s = 0; s < numSeqs; s++) {
            int state = data->getSite(p).getValue(s);
            if (state == gapChar) {
                meanGapProportion_++;
            } else {
                meanCharProportion_++;
            }
        }
    }

    meanGapProportion_ /= totalCharacters;
    meanCharProportion_ /= totalCharacters;

}

void bpp::alignStatistics::computeIndelDistributionAlignment() {
    size_t numSeqs = data->getNumberOfSequences();
    for(size_t i=0;i<numSeqs;i++){
        computeIndelDistributionForASequence(i);
    }

}

void bpp::alignStatistics::computeIndelDistributionForASequence(size_t seqID) {

    std::string sequence = data->getSequence(seqID).toString();
    std::regex wsaq_re("[-]+");

    std::regex exp("[-]+");
    std::smatch res;

    std::string::const_iterator searchStart( sequence.cbegin() );
    while ( regex_search( searchStart, sequence.cend(), res, exp ) )
    {
        searchStart += res.position() + res.length();
        indelDistribution_[seqID][res.length()]++;
    }


}

void bpp::alignStatistics::printIndelDistribution() {
    size_t numSeqs = data->getNumberOfSequences();
    double totalIndels = 0;
    std::map<int,int> summaryIndelDistribution;
    for(size_t i=0;i<numSeqs;i++) {
        //std::cout << "Indel distribution for sequence: "<< data->getSequence(i).getName() << std::endl;
        for (std::map<int, int>::iterator it = indelDistribution_[i].begin(); it != indelDistribution_[i].end(); ++it) {
            //cout << it->first << "\n"
            summaryIndelDistribution[it->first]++;
            //std::cout << it->first << "=" << it->second << std::endl;
        }
    }
    std::cout << "class" << "\t";
    for (std::map<int, int>::iterator it = summaryIndelDistribution.begin(); it != summaryIndelDistribution.end(); ++it) {
        if(it->first != 0) {

            std::string header = "["+std::to_string(it->first)+"]";
            std::cout << std::setw(10) << header;
        }
    }
    std::cout << std::endl;
    std::cout << "counts" << "\t";
    for (std::map<int, int>::iterator it = summaryIndelDistribution.begin(); it != summaryIndelDistribution.end(); ++it) {
        if( it->first != 0) {
            std::cout << std::setw(10) << it->second;
            totalIndels +=it->second;
        }
    }
    std::cout << std::endl;
    std::cout << "prop" << "\t";
    for (std::map<int, int>::iterator it = summaryIndelDistribution.begin(); it != summaryIndelDistribution.end(); ++it) {
        if( it->first != 0) {
            std::cout << std::setw(10) << it->second/totalIndels;
        }
    }
    std::cout << std::endl;

}

void bpp::alignStatistics::computeCScoreAlignment() {

    size_t numSites = data->getNumberOfSites();
    size_t numSeqs = data->getNumberOfSequences();

    for (size_t s1 = 0; s1 < numSeqs; s1++) {
        computeCScoreForASequence(s1);
        for (size_t s2 = 0; s2 < numSeqs; s2++) {
            computeCScoreForTwoSequences(s1, s2);
        }
    }

    for (size_t s = 0; s < numSites; s++) {
        computeCScoreForASite(s);
    }

    if (totalNumberOfUnambiguosChars_ > 0) {
        size_t numSites = data->getNumberOfSites();
        size_t numSeqs = data->getNumberOfSequences();

        CScoreAlignment_ = (double) totalNumberOfUnambiguosChars_ / (numSites * numSeqs);
    }

}

void bpp::alignStatistics::computeCScoreForASequence(size_t seqID) {

    totalNumberOfUnambiguosChars_ = 0;
    int gapChar = data->getAlphabet()->getGapCharacterCode();
    size_t numSites = data->getNumberOfSites();

    for (size_t s = 0; s < numSites; s++) {
        int state = data->getSequence(seqID).getValue(s);
        if (state != gapChar) {
            totalNumberOfUnambiguosChars_++;
            CScoreSequences_[seqID]++;
        }
    }

    CScoreSequences_[seqID] /= numSites;


}

void bpp::alignStatistics::computeCScoreForASite(size_t siteID) {

    totalNumberOfUnambiguosChars_ = 0;
    int gapChar = data->getAlphabet()->getGapCharacterCode();
    size_t numSeqs = data->getNumberOfSequences();

    for (size_t s = 0; s < numSeqs; s++) {
        int state = data->getSite(siteID).getValue(s);
        if (state != gapChar) {
            totalNumberOfUnambiguosChars_++;
            CScoreSites_[siteID]++;
        }
    }

    CScoreSites_[siteID] /= numSeqs;

}

void bpp::alignStatistics::computeCScoreForTwoSequences(size_t seq1, size_t seq2) {

    size_t numSites = data->getNumberOfSites();

    if (seq1 != seq2) {
        double score = pairSiteCounts_[seq1][seq2][siteClass::aligned_char] + pairSiteCounts_[seq1][seq2][siteClass::identical];
        CScorePairwise_[seq1][seq2] = score / numSites;
        ICScorePairwise_[seq1][seq2] = 1 - CScorePairwise_[seq1][seq2];

    } else {
        CScorePairwise_[seq1][seq2] = 1;
    }

}


