/***********************************************************************
* Licensed Materials - Property of Lorenzo Gatti
* Copyright (C) 2015-2019 by Lorenzo Gatti
************************************************************************
* This file is part of AliSt, a computer program whose purpose is to
* compute statistics on multi-sequence alignments.
*
* This software is based and extends the following libraries:
* - the Bio++ libraries
*   developed by the Bio++ Development Team <http://biopp.univ-montp2.fr>
*
* AliSt is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.
*
* AliST is a free software: you can redistribute it and/or modify it
* under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* You should have received a copy of the GNU Affero General Public
* License along with AliST. If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/**
 * @file main.cpp
 * @author Lorenzo Gatti
 * @date 18 07 2018
 * @version 1.0
 * @maintainer Lorenzo Gatti
 * @email lg@lorenzogatti.me
 * @status Development
 *
 * @brief
 * @details
 * @pre
 * @bug
 * @warning
 *
 * @see For more information visit: http://www.lorenzogatti.me
 */


#include <iostream>
#include <glog/logging.h>
#include <boost/filesystem.hpp>

#include <Bpp/Seq/Alphabet/AlphabetTools.h>
#include <Bpp/Seq/App/SequenceApplicationTools.h>
#include <Bpp/Seq/Io/Fasta.h>

#include "Version.hpp"
#include "AlistApplication.hpp"
#include "alignStatistics.hpp"
#include "OutputStats.hpp"

using namespace bpp;

int main(int argc, char *argv[]) {

    FLAGS_log_dir = ".";
    google::InitGoogleLogging(software::name.c_str());
    google::InstallFailureSignalHandler();

    try {


        bpp::AlistApplication app(argc,
                                  argv,
                                  std::string(software::name + " " + software::version),
                                  std::string(software::releasegitbranch + " " + software::releasegitref),
                                  std::string(software::releasedate + ", " + software::releasetime));

        if (argc < 2) {
            app.help();
            exit(0);
        } else {
            app.banner();
            app.startTimer();
        };

        std::string argInputFile = ApplicationTools::getAFilePath("input.sequence.file", app.getParams(), false, false, "", true, "", 1);
        std::string argOutputPrefix = ApplicationTools::getStringParameter("output.prefix", app.getParams(), "", "", true, true);
        std::string argAlphabet = ApplicationTools::getStringParameter("alphabet", app.getParams(), "DNA", "", true, true);
        bool argAlignment = ApplicationTools::getBooleanParameter("alignment", app.getParams(), false, "", true, 2);
        bool argShowTables = ApplicationTools::getBooleanParameter("showtables", app.getParams(), false, "", true, 2);
        int parPIDMethod = ApplicationTools::getIntParameter("method.pid", app.getParams(), 1, "", true, 2);




        // Alphabet used for all the computational steps (it can allows for gap extension)
        bpp::Alphabet *alphabet;
        // Genetic code
        std::unique_ptr<GeneticCode> gCode;
        // Codon alphabet ?
        bool codonAlphabet = false;

        if (argAlphabet.find("DNA") != std::string::npos || argAlphabet.find("Codon") != std::string::npos) {
            alphabet = new bpp::DNA();
        } else if (argAlphabet.find("Protein") != std::string::npos) {
            alphabet = new bpp::ProteicAlphabet();
        } else {}

        // This is additional to the alphabet instance
        if (argAlphabet.find("Codon") != std::string::npos) {
            alphabet = new CodonAlphabet(dynamic_cast<bpp::NucleicAlphabet *>(alphabet));
            codonAlphabet = true;
        }

        // Alphabet used for codon models
        if (codonAlphabet) {
            std::string codeDesc = ApplicationTools::getStringParameter("genetic_code", app.getParams(), "Standard", "", true, true);
            ApplicationTools::displayResult("Genetic Code", codeDesc);
            gCode.reset(bpp::SequenceApplicationTools::getGeneticCode(dynamic_cast<bpp::CodonAlphabet *>(alphabet)->getNucleicAlphabet(),
                                                                      codeDesc));
        }

        //////////////////////////////////////////////
        // DATA

        bpp::SequenceContainer *sequences = nullptr;
        bpp::SiteContainer *sites = nullptr;

        try {

            ApplicationTools::displayBooleanResult("Aligned sequences", !argAlignment);

            if (argAlignment) {

                // If the user requires the computation of an alignment, then the input file is made of unaligned sequences
                bpp::Fasta seqReader;
                sequences = seqReader.readSequences(argInputFile, alphabet);

            } else {

                VectorSiteContainer *allSites = SequenceApplicationTools::getSiteContainer(alphabet, app.getParams());
                sites = SequenceApplicationTools::getSitesToAnalyse(*allSites, app.getParams(), "", true, false, true, 1);
                delete allSites;
                //AlignmentUtils::checkAlignmentConsistency(*sites);
            }


        } catch (bpp::Exception &e) {
            LOG(FATAL) << "Error when reading sequence file due to: " << e.message();
        }

        alignStatistics::type pidmethod;

        switch(parPIDMethod){
            case 1:
                pidmethod =  alignStatistics::type::PID1;
                break;
            case 2:
                pidmethod =  alignStatistics::type::PID2;
                break;
            case 3:
                pidmethod =  alignStatistics::type::PID3;
                break;
            case 4:
                pidmethod =  alignStatistics::type::PID4;
                break;
        }

        ApplicationTools::displayResult("PID method", parPIDMethod);



        bpp::alignStatistics alignmentStats(*sites, pidmethod);
        alignmentStats.computeStatistics();
        ApplicationTools::displayMessage(" ");
        ApplicationTools::displayResult("Number of sequences", TextTools::toString(sites->getNumberOfSequences()));
        ApplicationTools::displayResult("Number of sites", TextTools::toString(sites->getNumberOfSites()));
        size_t seqpairs = sites->getNumberOfSequences() * (sites->getNumberOfSequences()-1)/2;
        ApplicationTools::displayResult("Number of pairs", TextTools::toString(seqpairs));
        ApplicationTools::displayMessage(" ");
        ApplicationTools::displayResult("Average pairwise Identity (PID)", TextTools::toString(alignmentStats.getMeanPID(),8));
        ApplicationTools::displayResult("Average GAP proportion", TextTools::toString(alignmentStats.getMeanGapProportion(),8));
        ApplicationTools::displayResult("Average CHAR proportion", TextTools::toString(alignmentStats.getMeanCharProportion(),8));
        ApplicationTools::displayMessage(" ");
        alignmentStats.computeCScoreAlignment();
        ApplicationTools::displayResult("Completeness (C) score align. (Ca)", TextTools::toString(alignmentStats.getCScoreAlignment(), 8));
        ApplicationTools::displayResult("Max C-score for sequences (Cr_max)", TextTools::toString(alignmentStats.getCr_max(), 8));
        ApplicationTools::displayResult("Min C-score for sequences (Cr_min)", TextTools::toString(alignmentStats.getCr_min(), 8));
        ApplicationTools::displayResult("Max C-score for sites (Cc_max)", TextTools::toString(alignmentStats.getCc_max(), 8));
        ApplicationTools::displayResult("Min C-score for sites (Cc_min)", TextTools::toString(alignmentStats.getCc_min(), 8));
        ApplicationTools::displayResult("Max C-score pairwise (Cij_max)", TextTools::toString(alignmentStats.getCij_max(), 8));
        ApplicationTools::displayResult("Min C-score pairwise (Cij_min)", TextTools::toString(alignmentStats.getCij_min(), 8));
        ApplicationTools::displayResult("Max I-score pairwise (Iij_max)", TextTools::toString(alignmentStats.getIij_max(), 8));
        ApplicationTools::displayResult("Min I-score pairwise (Iij_min)", TextTools::toString(alignmentStats.getIij_min(), 8));

        alignmentStats.computeIndelDistributionAlignment();

        ApplicationTools::displayMessage(" ");
        boost::filesystem::path absolutepath = boost::filesystem::complete(argInputFile);
        boost::filesystem::path argFileName(boost::filesystem::complete(argInputFile));
        std::string argExecutionPath = absolutepath.parent_path().string();
        std::string filename = argFileName.filename().stem().string();
        OutputStats::export2csv(&alignmentStats, filename, argOutputPrefix, argExecutionPath);

        if (argShowTables) {
            ApplicationTools::displayMessage(" ");
            ApplicationTools::displayMessage("---------- Pairwise Identity Matrix ---------");
            alignmentStats.printPIDMatrix();
            ApplicationTools::displayMessage(" ");
            ApplicationTools::displayMessage("---------- Indel distribution ---------");
            alignmentStats.printIndelDistribution();
            ApplicationTools::displayMessage(" ");
        }


        app.done();
        google::ShutdownGoogleLogging();
        exit(0);

    } catch (std::exception &e) {
        std::cout << e.what() << std::endl;
        google::ShutdownGoogleLogging();
        exit(1);
    }
}